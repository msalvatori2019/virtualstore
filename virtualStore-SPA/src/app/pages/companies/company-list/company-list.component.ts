import { Component, OnInit } from '@angular/core';
import { BaseResourceList } from 'src/app/shared/base-resource-list/base-resource-list.abstract';
import { Company } from '../shared/company.model';
import { CompanyService } from '../shared/company.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent extends BaseResourceList<Company> {

  constructor(private companyService: CompanyService) {
    
    super(companyService);
     this.columns = [
      { field: 'id', header: 'Código', width: '4', alignment: 'left'},
      { field: 'description', header: 'Descrição', width: '20', alignment: 'left'},
      { field: 'cnpj', header: 'Cnpj', width: '8', alignment: 'left'},
      { field: 'city', subfield: 'name', header: 'Descrição', width: '10', alignment: 'left'}         
    ];
  }

}
