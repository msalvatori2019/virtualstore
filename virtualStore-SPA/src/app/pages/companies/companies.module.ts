import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompaniesRoutingModule } from './companies-routing.module';
import { CompanyFormComponent } from './company-form/company-form.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [CompanyFormComponent, CompanyListComponent],
  imports: [
    CommonModule,
    SharedModule,
    CompaniesRoutingModule
  
  ]
})
export class CompaniesModule { }
