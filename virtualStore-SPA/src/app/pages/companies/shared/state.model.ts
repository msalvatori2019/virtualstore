import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';

export class State extends BaseResourceModel {
    constructor(
      public id?:number,
      public name?: string,
      public idCountry?: number) {
        
        super();
      }
      static fromJson(jsonData: any): State {
        return Object.assign(new State(), jsonData);
      }
}
