import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';


export class TypeCompany extends BaseResourceModel {
    constructor(
       id?:number,
       description?: string,
       code?: string
    )  {
        super();
    }

static fromJson(jsonData: any): TypeCompany {
  return Object.assign(new TypeCompany(), jsonData);
}
     
}

