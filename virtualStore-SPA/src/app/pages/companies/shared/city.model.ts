import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';
import { State } from './state.model';

export class City extends BaseResourceModel {
    constructor(
      public id?:number,
      public name?: string,
      public idState?: number,
      public state?:State) {
        
        super();
      }
      static fromJson(jsonData: any): City {
        return Object.assign(new City(), jsonData);
      }
}
