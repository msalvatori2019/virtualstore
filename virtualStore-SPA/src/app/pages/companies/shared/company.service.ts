
import { BaseResourceService } from 'src/app/shared/services/base-resource.service';
import { Injector, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from "src/environments/environment";
import { Company } from './company.model';
import { TypeCompany } from './typeCompany';


@Injectable({
  providedIn: 'root'
})
export class CompanyService extends BaseResourceService<Company> {

  constructor(protected injector: Injector) {
    super('company' , injector, Company.fromJson);
  }
  typeCompanies(): Observable<TypeCompany[]> {
    return this.http.get(environment.apiUrl+this.apiPath+'/getTypeCompanies').pipe(
      map(this.jsonDataToResources.bind(this)),
      catchError(this.handleError)
    )
  }

 }
