
import { BaseResourceService } from 'src/app/shared/services/base-resource.service';
import { Injector, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from "src/environments/environment";
import { Company } from './company.model';
import { City } from './city.model';
import { State } from './state.model';


@Injectable({
  providedIn: 'root'
})
export class CityService extends BaseResourceService<City> {

  constructor(protected injector: Injector) {
    super('city' , injector, City.fromJson);
  }
  cities(id:number): Observable<Company[]> {
    return this.http.get(environment.apiUrl+this.apiPath+'/'+id).pipe(
      map(this.jsonDataToResources.bind(this)),
      catchError(this.handleError)
    )
  }
  states(): Observable<State[]> {
    return this.http.get(environment.apiUrl+this.apiPath+'/getStates').pipe(
      map(this.jsonDataToResources.bind(this)),
      catchError(this.handleError)
    )
  }

 }
