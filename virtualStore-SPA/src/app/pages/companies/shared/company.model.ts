import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';
import { City } from './city.model';
import { State } from './state.model';
import { TypeCompany } from './typeCompany';

export class Company extends BaseResourceModel {
    constructor(
      public id?:number,
      public description?: string,
      public cnpj?: string,
      public typeCompany?: TypeCompany,
      public logradouro?: string,
	    public numero?: string,
      public latitude?: string,
      public longitude?: string,
      public state?: State,
      public city?: City,
      ) {
        
        super();
      }
      static fromJson(jsonData: any): Company {
        return Object.assign(new Company
            (), jsonData);
      }
}
