import { Component, OnInit, Injector } from '@angular/core';
import { BaseResourceForm } from 'src/app/shared/base-resource-form/base-resource-form.abstract';
import { Company } from '../shared/company.model';
import { City } from '../shared/city.model';
import { CompanyService } from '../shared/company.service';
import { Validators } from '@angular/forms';
import { CityService } from '../shared/city.service';
import { State } from '../shared/state.model';
import { TypeCompany } from '../shared/typeCompany';


@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.scss']
})
export class CompanyFormComponent extends BaseResourceForm <Company>  {
  cities: City[];
  states: State[];
  compare: string = 'id';
  typeCompanies: TypeCompany[];

  constructor(protected companyService: CompanyService, protected cityService: CityService, protected injector: Injector) {
    super(injector, new Company(), companyService, Company.fromJson);
    
    this.loadCities();
    this.loadStates();
    this.loadtypeCompanies();
    
  }


  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      id: [0],
      description: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
      cnpj: [null,[Validators.required, Validators.minLength(11)]],
      typeCompany: [null,Validators.required],
      logradouro: [null],
      numero: [null],
      latitude: [null],
      longitude: [null], 
      city: [null,Validators.required],
      state: [null,Validators.required]
     
    });
  }

  compareFn=this._compareFn.bind(this);

  _compareFn(a,b){
    if(a &&b ){
      return a[this.compare] === b[this.compare];
    }
  }

  private loadtypeCompanies(){
    this.companyService.typeCompanies().subscribe(
      typeCompanies => this.typeCompanies = typeCompanies
    );
  }
  
  private loadStates(){
    this.cityService.states().subscribe(
      states => this.states = states
    );   
  
  }


  private loadCities(){
    this.cityService.getAll().subscribe(
      cities => this.cities = cities
    );
  }

  onSelect() {
    this.cityService.getAll().subscribe(
      cities => this.cities = cities.filter((item)=> item.idState == this.resourceForm.value.state.id)
    );
   
  }

  protected creationPageTitle(): string {
    return "Cadastro de Empresas";
  }

  protected editionPageTitle(): string {
    const custoName = this.resource.description || "";
    return "Editando: ";
  }

}

