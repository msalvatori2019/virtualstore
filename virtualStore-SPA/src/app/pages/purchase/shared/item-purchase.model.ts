import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';


export class Purchase extends BaseResourceModel {
    constructor(
      public id?: number,
      public ca_digito?: string,
      public ca_descricao?: string,
      public ca_cbo?: string) {
        super();
      }
      static fromJson(jsonData: any): Purchase {
        return Object.assign(new Purchase(), jsonData);
      }
}
