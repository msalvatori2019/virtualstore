
import { BaseResourceService } from 'src/app/shared/services/base-resource.service';
import { Injector, Injectable } from '@angular/core';
import { Purchase } from './purchase.model';



@Injectable({
  providedIn: 'root'
})
export class PurchaseService extends BaseResourceService<Purchase> {

  constructor(protected injector: Injector) {
    super('pruchase' , injector, Purchase.fromJson);
  }
}
