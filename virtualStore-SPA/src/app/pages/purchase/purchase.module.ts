import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseFormComponent } from './purchase-form/purchase-form.component';
import { PurchaseListComponent } from './purchase-list/purchase-list.component';



@NgModule({
  declarations: [PurchaseFormComponent, PurchaseListComponent],
  imports: [
    CommonModule
  ]
})
export class PurchaseModule { }
