import { BaseResourceService } from 'src/app/shared/services/base-resource.service';
import { Injector, Injectable } from '@angular/core';
import { Customer } from './customer.model';


@Injectable({
  providedIn: 'root'
})
export class CustomerService extends BaseResourceService<Customer> {

  constructor(protected injector: Injector) {
    super('customer' , injector, Customer.fromJson);
  }
}
