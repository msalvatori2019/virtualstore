import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';

export class Customer extends BaseResourceModel {
    constructor(
      public id?: number,
      public name?: string,
      public cpf?: string,
      public rg?: string,
      public adress?: string) {
        super();
      }
      static fromJson(jsonData: any): Customer {
          return Object.assign(new Customer(), jsonData);
        }
    }
