import { BaseResourceService } from 'src/app/shared/services/base-resource.service';
import { Injector, Injectable } from '@angular/core';
import { Product } from './product.model';



@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseResourceService<Product> {

  constructor(protected injector: Injector) {
    super('product' , injector, Product.fromJson);
  }
}
