import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';

export class Product extends BaseResourceModel {
    constructor(
      public id?: number,
      public description?: string,
      public code?: string,
      public vlUnitary?: number,
      public validity?: Date) {
        super();
      }
      static fromJson(jsonData: any): Product {
          return Object.assign(new Product(), jsonData);
        }
    }
