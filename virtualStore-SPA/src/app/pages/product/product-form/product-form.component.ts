import { Component, OnInit, Injector } from '@angular/core';
import { Product } from '../shared/product.model';
import { BaseResourceForm } from 'src/app/shared/base-resource-form/base-resource-form.abstract';
import { ProductService } from '../shared/product.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent extends BaseResourceForm<Product> {

  constructor(protected productService: ProductService, protected injector: Injector) {
    super(injector, new Product(), productService, Product.fromJson);
  }
  compare = 'id';

  compareFn = this._compareFn.bind(this);

  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      id: [0],
      description: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
      code: [null],
      vlUnitary: [null],
      validity: [null]
    });
  }

   _compareFn(a, b) {
    if (a && b ) {
      return a[this.compare] === b[this.compare];
    }
  }

  protected creationPageTitle(): string {
    return 'Register  Products';
  }

  protected editionPageTitle(): string {
    const custoName = this.resource.description || '';
    return 'Editing: ';
  }


}
