import { BaseResourceModel } from './base-resource.model';
import { Role } from './role';

export class User extends BaseResourceModel {
  login: string;
  password: string;
  firstName: string;
  surname: string;
  active: boolean;
  email: string;
  token?: string;
  idRole: number;
  createDate: Date;
  updateDate: Date;
  userRole: Role;
  role: string[] = [];
  isMaster = false;
}