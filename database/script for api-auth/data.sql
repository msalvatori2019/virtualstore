
DELETE FROM oauth_client_details;

INSERT INTO security_user (username, email, password, activated)
SELECT * FROM (SELECT 'admin' as '1', 'admin@admin.com' as '2', '$2a$10$r0RFDmpneBVryx.ihHK9gu6FFJQi4nTxQUqzdSTvrPpaKZMxigqpy' as '3', 1 as t ) AS tmp
WHERE NOT EXISTS (
    SELECT username FROM security_user WHERE username = 'admin'
);

INSERT INTO security_authority (name)
SELECT top 1 * FROM (SELECT 'ROLE_USER' as '1') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM security_authority WHERE name = 'ROLE_USER'
) ;

INSERT INTO security_authority (name)
SELECT top 1 * FROM (SELECT 'ROLE_ADMIN' as '1') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM security_authority WHERE name = 'ROLE_ADMIN'
);

INSERT INTO security_user_authority (username, authority)
SELECT top 1 * FROM (SELECT 'admin' as '1', 'ROLE_USER' as '2') AS tmp
WHERE NOT EXISTS (
    SELECT username, authority FROM security_user_authority WHERE username = 'admin' and authority = 'ROLE_USER'
);

INSERT INTO security_user_authority (username, authority)
SELECT top 1 * FROM (SELECT 'admin' as '1', 'ROLE_ADMIN' as '2') AS tmp
WHERE NOT EXISTS (
    SELECT username, authority FROM security_user_authority WHERE username = 'admin' and authority = 'ROLE_ADMIN'
);