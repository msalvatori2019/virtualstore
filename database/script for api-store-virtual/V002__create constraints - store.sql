alter table Customer add constraint pk_customer_id primary key(ID_CUSTOMER);
alter table Product add constraint pk_product_id primary key(ID_PRODUCT);
alter table Purchase add constraint pk_purchase_id primary key(ID_PURCHASE);
alter table Purchase add constraint fk_purchasecustomer_idcustomer foreign key(ID_CUSTOMER)
references Customer(ID_CUSTOMER);
alter table item_purchase add constraint fk_itemprod_idproduct foreign key(ID_PRODUCT)
references Product(ID_PRODUCT);

create index idx_purchase_idcustomer on Purchase(ID_CUSTOMER)
create index idx_item_purchase_idproduct on item_Purchase(ID_PRODUCT)