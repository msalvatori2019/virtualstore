CREATE GENERATOR GEN_CUSTOMER;
CREATE GENERATOR GEN_PRODUCT;
CREATE GENERATOR GEN_PURCHASE;
CREATE GENERATOR GEN_ITEM_PURCHASE;

CREATE TRIGGER TG_INSERT_CUSTOMER FOR CUSTOMER
ACTIVE BEFORE INSERT POSITION 0
AS
BEGIN
new.id_customer = gen_id(gen_customer,1);
END

CREATE TRIGGER TG_INSERT_PRODUCT FOR PRODUCT
ACTIVE BEFORE INSERT POSITION 0
AS
BEGIN
new.id_product = gen_id(gen_product,1);
END

CREATE TRIGGER TG_INSERT_PURCHASE FOR PURCHASE
ACTIVE BEFORE INSERT POSITION 0
AS
BEGIN
new.id_purchase = gen_id(gen_purchase,1);
END

CREATE TRIGGER TG_INSERT_ITEM_PURCHASE FOR ITEM_PURCHASE
ACTIVE BEFORE INSERT POSITION 0
AS
BEGIN
new.id_item_purchase = gen_id(gen_item_purchase,1);
END