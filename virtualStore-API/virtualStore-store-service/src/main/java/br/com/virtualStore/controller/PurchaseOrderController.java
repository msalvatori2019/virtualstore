package br.com.virtualStore.controller;

import br.com.virtualStore.model.Purchase;
import br.com.virtualStore.service.PurchaseService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@CrossOrigin
@RequestMapping(value = "/api/purchases", produces = MediaType.APPLICATION_JSON_VALUE)
public class PurchaseOrderController extends BaseController<Purchase, PurchaseService, Long>{
}
