package br.com.virtualStore.repository;

import br.com.virtualStore.model.ItemPurchase;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ItemPurchaseRepository extends PagingAndSortingRepository<ItemPurchase,Long> {
}
