package br.com.virtualStore.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.validation.annotation.Validated;

public abstract class BaseService<E extends Serializable, RR extends PagingAndSortingRepository<E, ID>, ID extends Serializable> {

	@Autowired
	private RR repo;

	public E save(@Validated E item) {
		return repo.save(item);
	}

	public E findById(ID id) {
		return repo.findOne(id);
	}

	@Deprecated
	public Iterable<E> findAll() {
		return repo.findAll();
	}

	public Page<E> findAll(Pageable pageable) {
		return repo.findAll(pageable);
	}

	public void delete(ID id) {
		repo.delete(id);
	}
}
