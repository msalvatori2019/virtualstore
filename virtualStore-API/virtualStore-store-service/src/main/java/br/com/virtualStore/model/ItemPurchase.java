package br.com.virtualStore.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "ITEM_PURCHASE")
@Getter
@Setter
@NoArgsConstructor
@AttributeOverride(name = "id", column = @Column(name = "ID_ITEM_PURCHASE", nullable = false, columnDefinition = "integer"))
public class ItemPurchase extends BaseBean<Long> implements Serializable {

    @Column(name = "VL_UNITARY")
    @NotNull(message = "Value Unit required")
    @NumberFormat(pattern = "###,##0.00")
    @ApiModelProperty(value = "Value Unit  required", required = true)
    private BigDecimal vlUnitary;

    @Column(name = "QTD")
    @NotNull(message = "Amount required")
    @ApiModelProperty(value = "Amount  required", required = true)
    private Integer qtd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PRODUCT")
    @NotNull(message = "Product required")
    @ApiModelProperty(value = "Product required", required = true)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PURCHASE")
    @NotNull(message = "Purchase required")
    @ApiModelProperty(value = "Purchase required", required = true)
    private Purchase purchase;


}
