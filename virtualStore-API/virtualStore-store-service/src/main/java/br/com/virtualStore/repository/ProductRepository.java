package br.com.virtualStore.repository;

import br.com.virtualStore.model.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {


}
