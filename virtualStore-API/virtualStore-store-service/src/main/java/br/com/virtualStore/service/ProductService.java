package br.com.virtualStore.service;

import org.springframework.stereotype.Service;

import br.com.virtualStore.model.Product;
import br.com.virtualStore.repository.ProductRepository;

@Service
public class ProductService extends BaseService<Product, ProductRepository, Long> {
}
