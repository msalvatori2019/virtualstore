package br.com.virtualStore.service;

import br.com.virtualStore.model.ItemPurchase;
import br.com.virtualStore.model.Purchase;
import br.com.virtualStore.repository.PurchaseRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class PurchaseService extends BaseService<Purchase, PurchaseRepository, Long> {

    public boolean validatePurchase(Purchase purchase, List<ItemPurchase> listItensPurchase) {
        BigDecimal vl = BigDecimal.valueOf(0.00);
        for (ItemPurchase item : listItensPurchase) {
            vl = vl.add(item.getVlUnitary().multiply(BigDecimal.valueOf(item.getQtd())));
        }
        return purchase.getVlTotal().floatValue() == vl.floatValue() ? true : false;
    }
}