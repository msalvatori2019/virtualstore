package br.com.virtualStore.controller;

import br.com.virtualStore.model.Product;
import br.com.virtualStore.service.ProductService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
//@CrossOrigin
@RequestMapping(value = "/api/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController extends BaseController<Product, ProductService, Long>{
}
