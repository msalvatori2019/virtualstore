package br.com.virtualStore.repository;


import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.virtualStore.model.Customer;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {


}
