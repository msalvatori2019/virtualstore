package br.com.virtualStore.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "customer")
@Getter
@Setter

@NoArgsConstructor
@AttributeOverride(name = "id", column = @Column(name = "ID_CUSTOMER", nullable = false, columnDefinition = "integer"))
public class Customer extends BaseBean<Long> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "NAME")
	@NotEmpty(message = "Name required")
	@Length(min = 1, max = 100, message = "Name must contain between 1 and 100 characters")
	@ApiModelProperty(value = "Name required", required = true)
	private String name;

	@Column(name = "CPF")
	@NotEmpty(message = "Cpf required")
	@Length(min = 1, max = 16, message = "Cpf must contain between 1 and 16 characters")
	@ApiModelProperty(value = "Cpf required", required = true)
	private String cpf;

	@Column(name = "RG")
	@NotEmpty(message = "Rg required")
	@Length(min = 1, max = 11, message = "Rg must contain between 1 and 11 characters")
	@ApiModelProperty(value = "Rg required", required = true)
	private String rg;
	
	@Column(name = "ADRESS")
	@Length(min = 1, max = 150, message = "Adress must contain between 1 e 150  characters")
	@ApiModelProperty(value = "Adress required", required = false)
	private String adress;
	
	


	

}
