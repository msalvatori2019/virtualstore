package br.com.virtualStore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableEurekaClient
public class virtualStoreServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(virtualStoreServiceApplication.class, args);
	}
}
