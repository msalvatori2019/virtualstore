package br.com.virtualStore.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "product")
@Getter
@Setter
@NoArgsConstructor
@AttributeOverride(name = "id", column = @Column(name = "ID_PRODUCT", nullable = false, columnDefinition = "integer"))
public class Product extends BaseBean<Long> implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Description required")
	@Length(min = 1, max = 150, message = "Description must contain between 1 e 150 caracteres")
	@Column(name = "DESCRIPTION")
	private String description;
	
	@NotEmpty(message = "Code required")
	@Length(min = 1, max = 20, message = "Description must contain between 1 e 20 caracteres")
	@Column(name = "CODE")
	@ApiModelProperty(value = "Code required", required = true)
	private String code;
	
	@Column(name = "VL_UNITARY")
	@NumberFormat(pattern = "###,##0.00")
	@ApiModelProperty(value = "Code required", required = false)
	private BigDecimal vlUnitary;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Column(name = "DT_VALIDITY")
	@NotNull(message = "Date  required")
	@ApiModelProperty(value = "Date required", required = true)
	private Date validity;


	
}
