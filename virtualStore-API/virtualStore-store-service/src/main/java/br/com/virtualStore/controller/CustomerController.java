package br.com.virtualStore.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.virtualStore.model.Customer;
import br.com.virtualStore.service.CustomerService;

@RestController
//@CrossOrigin
@RequestMapping(value = "/api/customers", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController extends BaseController<Customer, CustomerService, Long>{
}
