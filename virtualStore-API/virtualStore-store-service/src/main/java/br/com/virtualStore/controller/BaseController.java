package br.com.virtualStore.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.virtualStore.service.BaseService;

public abstract class BaseController<E extends Serializable, SS extends BaseService<E, ?, ID>, ID extends Serializable> {

	@Autowired
	private SS service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<E> save(@RequestBody E item) {
		return ResponseEntity.ok(service.save(item));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<E> findById(@PathVariable("id") ID id) {
		return ResponseEntity.ok(service.findById(id));
	}
//
//	@RequestMapping(method = RequestMethod.GET)
//	public ResponseEntity<Iterable<E>> findAll() {
//		return ResponseEntity.ok().body(service.findAll());
//	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<E>> findAll(Pageable pageable) {
		return ResponseEntity.ok().body(service.findAll(pageable));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable("id") ID id) {
		service.delete(id);
		return ResponseEntity.ok().build();
	}
}
