package br.com.virtualStore.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "purchase")
@Getter
@Setter
@NoArgsConstructor
@AttributeOverride(name = "id", column = @Column(name = "ID_PURCHASE", nullable = false, columnDefinition = "integer"))

public class Purchase extends BaseBean<Long> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "Vl_TOTAL")
    @NotNull(message = "VlTotal required")
    @NumberFormat(pattern = "###,##0.00")
    @ApiModelProperty(value = "Value Total required", required = true)
    private BigDecimal vlTotal;

    @Column(name = "QTD")
    @NotNull(message = "QTD required")
    @ApiModelProperty(value = "Amount required", required = true)
    private Integer qtd;

    @DateTimeFormat(iso = ISO.DATE_TIME)
    @Column(name = "DT_PURCHASE")
    @NotNull(message = "Date  required")
    @ApiModelProperty(value = "Date required", required = true)
    private Date dtPurchase;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CUSTOMER")
    private Customer customer;

}
