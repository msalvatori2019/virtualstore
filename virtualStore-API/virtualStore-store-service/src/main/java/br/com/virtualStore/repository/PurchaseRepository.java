package br.com.virtualStore.repository;

import br.com.virtualStore.model.Purchase;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PurchaseRepository extends PagingAndSortingRepository<Purchase, Long> {

}
