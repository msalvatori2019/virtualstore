package br.com.virtualStore.service;

import org.springframework.stereotype.Service;

import br.com.virtualStore.model.Customer;
import br.com.virtualStore.repository.CustomerRepository;

@Service
public class CustomerService extends BaseService<Customer, CustomerRepository, Long> {
}
