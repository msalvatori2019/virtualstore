package br.com.virtualStore.service;

import br.com.virtualStore.model.ItemPurchase;
import br.com.virtualStore.model.Purchase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseServiceTests {

    @Autowired
    PurchaseService purchaseService;

    @Before
    public void setUp() {}

    @Test
    public void validateValuePurchase() throws ParseException {
        assertEquals(true, purchaseService.validatePurchase(this.getDataPurchase(), getListItemPurchase()));

    }
    private Purchase getDataPurchase() throws ParseException {
        Purchase purchase = new Purchase();
        purchase.setQtd(1);
        BigDecimal vl1 = new BigDecimal(200.00);
        purchase.setVlTotal(vl1);
        String data = "10/09/2020";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        purchase.setDtPurchase(sdf.parse(data));
        return purchase;
    }
    private List<ItemPurchase> getListItemPurchase() throws ParseException {
        List<ItemPurchase> listItemPurchase =  new ArrayList<ItemPurchase>();
        ItemPurchase itemPurchase = new ItemPurchase();
        itemPurchase.setQtd(2);
        BigDecimal vl1 = new BigDecimal(25.00);
        itemPurchase.setVlUnitary(vl1);
        listItemPurchase.add(itemPurchase);

        ItemPurchase itemPurchase2 = new ItemPurchase();
        itemPurchase2.setQtd(2);
        BigDecimal vl2 = new BigDecimal(25.00);
        itemPurchase2.setVlUnitary(vl2);
        listItemPurchase.add(itemPurchase2);

        ItemPurchase itemPurchase3 = new ItemPurchase();
        itemPurchase3.setQtd(2);
        BigDecimal vl3 = new BigDecimal(50.00);
        itemPurchase3.setVlUnitary(vl3);
        listItemPurchase.add(itemPurchase3);

        return listItemPurchase;
    }
}
