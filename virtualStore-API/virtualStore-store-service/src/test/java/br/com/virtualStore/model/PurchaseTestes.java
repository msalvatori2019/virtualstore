package br.com.virtualStore.model;

import br.com.virtualStore.interfaces.IrepositoryTests;
import br.com.virtualStore.repository.CustomerRepository;
import br.com.virtualStore.repository.ItemPurchaseRepository;
import br.com.virtualStore.repository.ProductRepository;
import br.com.virtualStore.repository.PurchaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;

/**Class  object Purchase tests will be carried out to insert, locate, update and delete.
 * @author Marcelo Salvatori
 * @version 1.00
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseTestes implements IrepositoryTests {

    @Autowired
    PurchaseRepository repository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ItemPurchaseRepository itemPurchaseRepository;

    Purchase purchase;

    Customer customer;

    Product product;

    ItemPurchase itemPurchase;

    @Before
    public void setUp() {}

    @Transactional
    @Override
    @Test
    public void saveOrUpdate() throws ParseException {

        /** insert  Customer
         * @author Marcelo Salvatori
         * @param CPF - unique customer identifier type string
         * @param Name - Name Customer type string
         * @param Rg - RG Customer type string
         * @param Adress - adress customer
         * @return Customer - Insertion result value
         */
        this.customer = new Customer();
        this.customer.setCpf("88888888888");
        this.customer.setName("Fulano 1");
        this.customer.setRg("12345678911");
        this.customer.setAdress("center");
        this.customer = customerRepository.save(this.customer);
        assertNotNull(this.customer);

        /** insert  Purchase
         * @author Marcelo Salvatori
         * @param VlTOTAL - Total purchase value
         * @param QTD - Total purchase amount
         * @param DTPURCHASE -  Purchase Date
         * @param Customer - Customer purchase
         * @return Purchase - Insertion result value
         */
        this.purchase = new Purchase();
        this.purchase.setQtd(1);
        BigDecimal vl1 = new BigDecimal(20.00);
        this.purchase.setVlTotal(vl1);
        String data = "10/09/2020";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.purchase.setDtPurchase(sdf.parse(data));
        this.purchase.setCustomer(this.customer);
        this.purchase = repository.save(this.purchase);

        assertNotNull(this.purchase);

        /** insert  Product
         * @author Marcelo Salvatori
         * @param Code - Product identifier
         * @param Description - Product description
         * @param Validity - Expiration date
         * @param VlUnitary - adress customer
         * @return Product - Insertion result value
         */
        this.product = new Product();
        this.product.setCode("LOP123");
        this.product.setDescription("Washing machine");
        String data2 = "12/10/2020";
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
        this.product.setValidity(sdf2.parse(data2));
        BigDecimal vl2 = new BigDecimal(15.00);
        this.product.setVlUnitary(vl2);
        this.product = productRepository.save(this.product);
        assertNotNull(this.product);

        /** insert  ItemPurchase
         * @author Marcelo Salvatori
         * @param VlUnitary - Unit value of purchase
         * @param QTD -  Amount value of purchase
         * @param Product - Product Item purchase
         * @param Purchase - Purchase ItemPurchase
         * @return ItemPurchase - Insertion result value
         */
         this.itemPurchase = new ItemPurchase();
         this.itemPurchase.setQtd(1);
         BigDecimal vl3 = new BigDecimal(25.00);
         this.itemPurchase.setVlUnitary(vl3);
         this.itemPurchase.setPurchase(this.purchase);
         this.itemPurchase.setProduct(this.product);
         this.itemPurchase = itemPurchaseRepository.save(this.itemPurchase);
         assertNotNull(this.itemPurchase);


        /** Update  Purchase
         * @author Marcelo Salvatori
         * @param VlTOTAL - Total purchase value
         * @param QTD - Total purchase amount
         * @param DTPURCHASE -  Purchase Date
         * @param Customer - Customer purchase
         * @return Purchase - Change result value
         */

        this.purchase.setQtd(2);
        BigDecimal vl4 = new BigDecimal(30.00);
        this.purchase.setVlTotal(vl1);
        String data4 = "20/09/2020";
        SimpleDateFormat sdf4 = new SimpleDateFormat("dd/MM/yyyy");
        this.purchase.setDtPurchase(sdf4.parse(data));
        this.purchase = repository.save(this.purchase);
        this.purchase.setCustomer(this.customer);
        Purchase purchaseUpdate = new Purchase();
        purchaseUpdate = this.purchase;
        assertEquals(purchaseUpdate,this.purchase);

        /** update  ItemPurchase
         * @author Marcelo Salvatori
         * @param VlUnitary - Unit value of purchase
         * @param QTD -  Amount value of purchase
         * @param Product - Product Item purchase
         * @param Purchase - Purchase ItemPurchase
         * @return ItemPurchase - Change result value
         */
        this.itemPurchase.setQtd(2);
        BigDecimal vl5 = new BigDecimal(50.00);
        this.itemPurchase.setVlUnitary(vl5);
        this.itemPurchase.setPurchase(this.purchase);
        this.itemPurchase.setProduct(this.product);
        ItemPurchase itemPurchaseUpdate = new ItemPurchase();
        itemPurchaseUpdate = this.itemPurchase;
        this.itemPurchase = itemPurchaseRepository.save(this.itemPurchase);
        assertEquals(purchaseUpdate,this.purchase);

        /** search by id ItemPurchase
         * @author Marcelo Salvatori
         */
        ItemPurchase result = itemPurchaseRepository.findOne(this.itemPurchase.getId());
        assertEquals(result,this.itemPurchase);

        /** search by id Purchase
         * @author Marcelo Salvatori
         */
        Purchase result2 = repository.findOne(this.purchase.getId());
        assertEquals(result2,this.purchase);


        /**delete ItemPurchase validate
         * @author Marcelo Salvatori
         * @return true
         */
        Long code = result.getId();
        itemPurchaseRepository.delete(result);
        assertNull(repository.findOne(code));

        /**delete Purchase validate
         * @author Marcelo Salvatori
         * @return true
         */
        Long code2 = result2.getId();
        repository.delete(result2);
        assertNull(repository.findOne(code2));

    }
}