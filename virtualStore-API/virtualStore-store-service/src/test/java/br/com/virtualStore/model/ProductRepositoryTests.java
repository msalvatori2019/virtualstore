package br.com.virtualStore.model;

import br.com.virtualStore.interfaces.IrepositoryTests;
import br.com.virtualStore.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;


/**Class  object Product tests will be carried out to insert, locate, update and delete.
 * @author Marcelo Salvatori
 * @version 1.00
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTests implements IrepositoryTests {

    @Autowired
    ProductRepository repository;

    Product product;

    @Before
    public void setUp() {}

    @Transactional
    @Override
    @Test
    public void saveOrUpdate() throws ParseException {

        /**insert  Product
         * @author Marcelo Salvatori
         * @param Code - Product identifier
         * @param Description - Product description
         * @param Validity - Expiration date
         * @param VlUnitary - adress customer
         * @return Product - Insertion result value
         */
        this.product = new Product();
        this.product.setCode("LOP123");
        this.product.setDescription("Washing machine");
        String data = "10/10/2020";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.product.setValidity(sdf.parse(data));
        BigDecimal vl1 = new BigDecimal(15.00);
        this.product.setVlUnitary(vl1);
        this.product = repository.save(this.product);
        assertNotNull(this.product);

        /**update Product
         * @author Marcelo Salvatori
         * @param Code - Product identifier
         * @param Description - Product description
         * @param Validity - Expiration date
         * @param VlUnitary - adress customer
         * @return Product  - Change result value
         */
        this.product = new Product();
        this.product.setCode("LOP123");
        this.product.setDescription("Washing machine");
        String data2 = "10/10/2025";
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
        this.product.setValidity(sdf2.parse(data2));
        BigDecimal vl2 = new BigDecimal(16.00);
        this.product.setVlUnitary(vl2);
        Product productUpdate = new Product();
        productUpdate = this.product;
        this.product = repository.save(this.product);
        assertEquals(productUpdate,this.product);

        /** search by id Product
         * @author Marcelo Salvatori
         * @return Product
         */
        Product result = repository.findOne(this.product.getId());
        assertEquals(result,this.product);

        /**delete Customer validate
         * @author Marcelo Salvatori
         * @return true
         */
        Long code = result.getId();
        repository.delete(result);
        assertNull(repository.findOne(code));
    }


}
