package br.com.virtualStore.model;


import br.com.virtualStore.interfaces.IrepositoryTests;
import br.com.virtualStore.repository.CustomerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**Class  object Customer tests will be carried out to insert, locate, update and delete.
 * @author Marcelo Salvatori
 * @version 1.00
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTests implements IrepositoryTests {

	@Autowired
	CustomerRepository repository;

	Customer customer;

	@Before
	public void setUp() {}
	
	@Transactional
	@Override
	@Test
	public void saveOrUpdate() {

		/**insert  Customer
		 * @author Marcelo Salvatori
		 * @param CPF - unique customer identifier type string
		 * @param Name - Name Customer type string
		 * @param Rg - RG Customer type string
		 * @param Adress - adress customer
		 * @return Customer - Insertion result value
		 */
		this.customer = new Customer();
		this.customer.setCpf("88888888888");
		this.customer.setName("Fulano 1");
		this.customer.setRg("12345678911");
		this.customer.setAdress("center");
		this.customer = repository.save(this.customer);
		assertNotNull(this.customer);

		/**update Customer
		 * @author Marcelo Salvatori
		 * @param CPF - unique customer identifier type string
		 * @param Name - Name Customer type string
		 * @param Rg - RG Customer type string
		 * @param Adress - adress customer
		 * @return Customer  - Change result value
		 */
		this.customer.setCpf("99999999");
		this.customer.setName("Fulano 2");
		this.customer.setRg("12345678912");
		this.customer.setAdress("region");
		Customer customerUpdate = new Customer();
		customerUpdate = this.customer;
		this.customer = repository.save(this.customer);
		assertEquals(customerUpdate,this.customer);

		/** search by id Customer
		 * @author Marcelo Salvatori
		 * @return Customer
		 */
		Customer result = repository.findOne(this.customer.getId());
		assertEquals(result,this.customer);

		/**delete Customer validate
		 * @author Marcelo Salvatori
		 * @return true
		 */
		Long code = result.getId();
		repository.delete(result);
		assertNull(repository.findOne(code));
	}




}