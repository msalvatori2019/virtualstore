package br.com.virtualStore.controller;

import static org.springframework.http.ResponseEntity.ok;

import java.security.Principal;

import javax.servlet.http.HttpServletResponse;

import br.com.virtualStore.model.User;
import br.com.virtualStore.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/")
public class AuthController {
	
	@Autowired
	private AuthService authService;
	
	@RequestMapping("/user")
	public Principal getCurrentLoggedInUser(Principal user) {
		return user;
	}
	
	@PostMapping("/sigmin")
	public ResponseEntity logar(@Validated @RequestBody User user, HttpServletResponse response) {
		
		try {
			String username="", password="";
			username = user.getUsername();
			password = user.getPassword();			 
			return ok(authService.getToken(username, password));
		} catch (Exception e) {
			throw new BadCredentialsException("invalido");
		}
		
	}

}
