package br.com.virtualStore.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.virtualStore.model.Token;

@Service
public class AuthService {

	@Value("${spring.hostname}")
	private String hostname;

	@Value("${server.port}")
	private String port;

	@Value("${spring.token}")
	private String token;

	private ResponseEntity<String> response = null;

	@Autowired
	private UserDetailsService userDetailsService;

	Token tokenObj = new Token();

	public Map<Object, Object> getToken(String username, String password)
			throws JsonParseException, JsonMappingException, IOException {
		UserDetails userDetails;
		userDetails = userDetailsService.loadUserByUsername(username);

		UsernamePasswordAuthenticationToken userToken = new UsernamePasswordAuthenticationToken(userDetails,
				userDetails.getPassword(), userDetails.getAuthorities());

		if (userToken.isAuthenticated()) {

			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", "Basic " + "Y29kZXJlZjokMmEkMTAkcDlQazBmUU5BUVNlc0k0dnV2S0EwT1phbkREMg==");

			HttpEntity<String> request = new HttpEntity<String>(headers);

			String access_token_url = hostname.concat(":").concat(port).concat(token);
			access_token_url += "?grant_type=password&username=" + username + "&password=" + password;

			response = restTemplate.exchange(access_token_url, HttpMethod.POST, request, String.class);
			response.getBody();

			ObjectMapper mapper = new ObjectMapper();
			String json = response.getBody();
			tokenObj = mapper.readValue(json, Token.class);

			Map<Object, Object> model = new HashMap<>();
			model.put("username", username);
			model.put("token", tokenObj.getAccess_token());
			return model;
		} else {
			return null;
		}

	}
}
